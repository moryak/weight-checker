Для запуска проекта необходимо:

* Установить Docker, Docker compose, Git,
* Склонировать репозиторий
* В корне проекта выполнить `docker-compose up -d`
* Затем `docker exec -it weight bash`
* Внутри контейнера выполнить `composer install`
* Приложение станет доступно по url [http://localhost:8000](http://localhost:8000)