<?php
namespace App\Controller;

use App\Service\TrackerDataService;
use Envms\FluentPDO\Exception;

/**
 * Class Controller
 *
 * @package App\Controller
 */
class Controller
{
    /**
     * Action для базовой страницы
     *
     * @return void
     */
    public function index(): void
    {
        print template('base');
    }

    /**
     * Action для трекера веса
     *
     * @param array $request
     * @return void
     * @throws Exception
     */
    public function tracker(array $request): void
    {
        $trackerDataService = new TrackerDataService();

        print template('base', ['data' => $trackerDataService->getDataJson($request)]);
    }
}
