<?php

declare(strict_types=1);

namespace App\Migrations;

use DateInterval;
use DatePeriod;
use DateTime;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191001235059 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription() : string
    {
        return '';
    }

    /**
     * @param Schema $schema
     * @throws Exception
     */
    public function up(Schema $schema) : void
    {
        $values = '';

        $period = new DatePeriod(
            new DateTime('2019-09-01'),
            new DateInterval('P1D'),
            new DateTime('2019-11-01')
        );

        foreach ($period as $key => $weight_date) {
            $weight_date = $weight_date->format('Y-m-d');
            $values .= "('$weight_date', " . random_int(300, 3000) . '), ';
        }

        $values = trim($values, ', ');

        $this->addSql(
            "INSERT INTO tracker (weight_date, weight_value) VALUES
             $values"
        );
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE FROM tracker');
    }
}
