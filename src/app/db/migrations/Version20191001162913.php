<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191001162913 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription() : string
    {
        return '';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema) : void
    {
        $this->addSql(
            'CREATE TABLE tracker
                (
                    id INT AUTO_INCREMENT NOT NULL,
                    weight_date DATETIME,
                    weight_value MEDIUMINT DEFAULT NULL,
                    PRIMARY KEY(id)
                )'
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE tracker');
    }
}
