<?php
namespace App;

use App\Service\LoggerService;
use Exception;
use Monolog\Logger;

require_once 'functions.php';

/**
 * Class Kernel
 *
 * @package App
 */
class Kernel
{
    /**
     * Старт приложения
     *
     * @return mixed
     * @throws Exception
     */
    public function run()
    {
        $requestUri = $_SERVER['REQUEST_URI'];

        if (preg_match('/\.(?:png|jpg|jpeg|gif|ico)$/', $requestUri)) {
            return false;
        }

        $route = route($requestUri);

        if (!$route) {
            LoggerService::getLogger('app', Logger::WARNING)->warning(
                'Trying to get undefined uri ' . $requestUri
            );

            redirect('/');
        }

        $controller = new $route['controller']();
        $action = $route['action'];

        return $controller->$action($_REQUEST);
    }
}
