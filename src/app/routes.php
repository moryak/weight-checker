<?php

use App\Controller\Controller;

return [
    '/' => ['controller' => Controller::class, 'action' => 'index'],
    '/tracker' => ['controller' => Controller::class, 'action' => 'tracker']
];
