<?php

/**
 * Рендеринг шаблона
 *
 * @param string $name
 * @param array $args
 * @return string
 */
function template(string $name, array $args = []): string
{
    $file = __DIR__ . '/views/' . $name . '.php';

    if (!file_exists($file)) {
        return '';
    }

    extract($args, EXTR_SKIP);

    ob_start();
    include $file;

    return ob_get_clean();
}

/**
 * Получение контроллера и экшена
 *
 * @param string $name
 * @return bool|string
 */
function route(string $name) {
    $routes = __DIR__ . '/routes.php';

    if (!file_exists($routes)) {
        return false;
    }

    $routes = require $routes;

    return array_key_exists($name, $routes) ? $routes[$name] : false;
}

/**
 * Редирект
 *
 * @param string $url
 * @return void
 */
function redirect($url): void {
    header('Location: ' . $url);

    exit();
}
