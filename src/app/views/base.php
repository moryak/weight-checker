<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Weight Checker</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( ".datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
        } );
    </script>
</head>
<body>
<form action="/tracker" method="post">
    <p>Дата от:<input type="text" name="date_start" class="datepicker" ></p>
    <br>
    <p>Дата от:<input type="text" name="date_end" class="datepicker" ></p>
    <p>
        <select name="detalization">
            <option selected value="">Выберите детализацию</option>
            <option value="day">По дням</option>
            <option value="week">По неделям</option>
            <option value="month">По месяцам</option>
        </select>
    </p>
    <input type="submit" value="Отправить">
</form>

<?php if ($data) {
    include 'chart.php';
} ?>

</body>
</html>
