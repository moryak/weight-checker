
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


<script>
    let response = JSON.parse('<?php echo $data; ?>');
    let categories = [];
    let values = [];
    let type = '';

    if (response.detalization) {
        type = response.detalization.type;
        let data = response.detalization.data;

        prepareValuesForChart(data, type);
    }

    function prepareValuesForChart(data, type)
    {
        data.forEach(function(element) {
            if (type === 'day') {
                categories.push("" + element.date + "");
                values.push(parseInt(element.weight));

                return;
            }
            categories.push("" + element[type][0].date + " / " + element[type][element[type].length-1].date + "");
            values.push(parseInt(element.average));
        });
    }


    Highcharts.chart('container', {
          chart: {
            type: 'line'
          },
          title: {
            text: 'Среднее значение: ' + response.totalAverage
          },
          subtitle: {
            text: 'Weight checker'
          },
          xAxis: {
            categories: categories
          },
          yAxis: {
            title: {
              text: 'Вес в граммах'
            }
          },
          plotOptions: {
            line: {
              dataLabels: {
                enabled: true
              },
              enableMouseTracking: false
            }
          },
          series: [{
            name: type,
            data: values
          }]
    });
</script>
