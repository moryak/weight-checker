<?php
namespace App\Service;

use Envms\FluentPDO\Query;
use Exception;
use Monolog\Logger;
use PDO;
use PDOException;

/**
 * Class DB
 * @package App
 */
class DB
{
    /**
     * DSN
     *
     * @var string
     */
    private static $dsn = 'mysql:dbname=weight;host=weight-mysql';

    /**
     * БД пользователь
     *
     * @var string
     */
    private static $user = 'weight';

    /**
     * БД пароль
     *
     * @var string
     */
    private static $pass = 'weight';

    /**
     * @var PDO
     */
    private static $pdo;

    /**
     * Подключение к БД
     *
     * @return PDO
     * @throws Exception
     */
    private static function getPdo(): PDO
    {
        if (!self::$pdo) {
            try {
                self::$pdo = new PDO(
                    self::$dsn,
                    self::$user,
                    self::$pass,
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")
                );
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            } catch (PDOException $e) {
                LoggerService::getLogger('app', Logger::ERROR)->error(
                    $e->getMessage()
                );
                exit('Ошибка подключения к БД: ' . $e->getMessage());
            }
        }

        return self::$pdo;
    }

    /**
     * Get FluentPdo
     *
     * @return Query
     * @throws Exception
     */
    public static function query(): Query
    {
        return new Query(self::getPdo());
    }
}