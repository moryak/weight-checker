<?php
namespace App\Service;

use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerService
{
    /**
     * @var array
     */
    private static $loggers;

    /**
     * Получение объекта класса работы с логами
     *
     * @param string $name
     * @param string|int $debugLevel
     * @return Logger
     * @throws Exception
     */
    public static function getLogger(string $name, $debugLevel): Logger
    {
        if (!isset(self::$loggers[$name])) {
            $logger = new Logger($name);
            $logger->pushHandler(new StreamHandler(__DIR__ . '/../../tmp/app.log', $debugLevel));

            self::$loggers[$name] = $logger;
        }

        return self::$loggers[$name];
    }
}