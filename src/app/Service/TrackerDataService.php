<?php
namespace App\Service;

use App\Model\Tracker;
use Envms\FluentPDO\Exception;

/**
 * Class TrackerDataService
 * @package App\Service
 */
class TrackerDataService
{
    /**
     * @var string
     */
    private const DETALIZATION_TYPE_DAY = 'day';

    /**
     * @var string
     */
    private const DETALIZATION_TYPE_WEEK = 'week';

    /**
     * @var string
     */
    private const DETALIZATION_TYPE_MONTH = 'month';

    /**
     * @var Tracker
     */
    private $model;

    /**
     * TrackerDataService constructor.
     */
    public function __construct()
    {
        $this->model = new Tracker();
    }

    /**
     * Получение массива данных
     *
     * @param array $request
     * @return array
     * @throws Exception
     */
    public function getData(array $request): array
    {
        if ($request['date_start'] && $request['date_end']) {
            $data = $this->model->getFromRange($request['date_start'], $request['date_end']);
        } else {
            $data = $this->model->getAll();
        }

        $response = [
            'totalAverage' => $this->model->averageWeight($data)
        ];

        if ($request['detalization']) {
            $response['detalization'] = $this->detalization($data, $request['detalization']);
        }

        return $response;
    }

    /**
     * Получение данных в формате json
     *
     * @param array $request
     * @return string
     * @throws Exception
     */
    public function getDataJson(array $request): string
    {
        return json_encode($this->getData($request));
    }

    /**
     * Детализация данных
     *
     * @param $data
     * @param $detalizationType
     * @return array
     */
    private function detalization($data, $detalizationType): ?array
    {
        $detalization = [];

        switch ($detalizationType) {
            case self::DETALIZATION_TYPE_DAY:
                $detalization['type'] = self::DETALIZATION_TYPE_DAY;
                $detalization['data'] = $data;

                break;
            case self::DETALIZATION_TYPE_WEEK:
                $weeks = [];
                $chunked = array_chunk($data, 7);

                foreach ($chunked as $chunk) {
                    $weeks[] = [
                        'average' => $this->model->averageWeight($chunk),
                        'week' => $chunk
                    ];
                }

                $detalization['type'] = self::DETALIZATION_TYPE_WEEK;
                $detalization['data'] = $weeks;

                break;
            case self::DETALIZATION_TYPE_MONTH:
                $months = [];
                $chunked = array_chunk($data, 30);

                foreach ($chunked as $chunk) {
                    $months[] = [
                        'average' => $this->model->averageWeight($chunk),
                        'month' => $chunk
                    ];
                }

                $detalization['type'] = self::DETALIZATION_TYPE_MONTH;
                $detalization['data'] = $months;

                break;
        }

        return $detalization;
    }
}