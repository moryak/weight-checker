<?php
namespace App\Model;

use App\Service\DB;
use Envms\FluentPDO\Exception;

/**
 * Class Tracker
 * @package App\Model
 */
class Tracker
{
    /**
     * @var string
     */
    private $weight_date = 'weight_date';

    /**
     * @var string
     */
    private $weight_value= 'weight_value';

    /**
     * @var string
     */
    private $table = 'tracker';

    /**
     * Получить все данные из БД
     *
     * @return array
     * @throws Exception
     */
    public function getAll(): array
    {
        return $this->normalizeDbResponse(DB::query()->from($this->table));
    }

    /**
     * Получить данные из БД за период
     *
     * @param string $start
     * @param string $end
     * @return array
     * @throws Exception
     */
    public function getFromRange(string $start, string $end): array
    {
        $result = DB::query()->from($this->table)->where(
            [
                'weight_date > ?' => $start,
                'weight_date < ?' => $end
            ]
        );

        return $this->normalizeDbResponse($result);
    }

    /**
     * Среднией вес
     *
     * @param array $weights
     * @return float|int
     */
    public function averageWeight(array $weights)
    {
        $amount = count($weights);
        $weightSum = 0;

        foreach ($weights as $date => $value) {
            $weightSum += $value['weight'];
        }

        return intdiv($weightSum, $amount);
    }

    /**
     * Нормализация ответа из БД
     *
     * @param object $data
     * @return array
     */
    private function normalizeDbResponse($data): array {
        $dbResponse = [];

        foreach($data as $row){
            $dbResponse[] = [
                    'date' => date_format(date_create($row[$this->weight_date]), 'Y-m-d'),
                    'weight' => $row[$this->weight_value]
                ];
        }

        return $dbResponse;
    }
}