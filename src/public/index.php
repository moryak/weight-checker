<?php

require_once '../vendor/autoload.php';

use App\Kernel;

$controller = new Kernel();
$controller->run();
